/*
 * Utworzyć program, który będzie przemieniał podany ciąg znakowy na kod Morse'a,a kode Morse'a na standardowy tekst
 */
public class MorseCode {
									//A		B			C		D		E		F
	public static String[] morse = {"• —","— • • •", "— • — •","— • •","•","• • — •",
			//G			H		I		J		K			L		M		
			"— — •","• • • •","• •","• — — —","— • —","• — • •","— —",
			//N		O		P			Q		R		S		T
			"— •","— — —","• — — •","— — • —","• — •","• • •","—",
			//U			V		W		X			Y			Z
			"• • —","• • • —","• — —","— • • —", "— • — —","— — • •"
	};
	
	public static String[] morseConsole = 
		{".-","-...", "-.-.","-..",".","..-.",
			//G			H		I		J		K			L		M		
			"--.","....","..",".---","-.-",".-..","--",
			//N		O		P			Q		R		S		T
			"-.","---",".--.","--.-",".-.","...","-",
			//U			V		W		X			Y			Z
			"..-","...-",".--","-..-", "-.--","--.."
	};
	
	public static String[][] morseStatic = {
				// 0	 			// 1	  //2
			{"• — • • —",         ".-..-",     " "},  //element 0
			{"• • • — • —",       "...-.-",    "."},  //element 1
			{"• • • — — — • • •", "...---...", "SOS"},//element 2
	};
	
	public static char[] morseSign = {'•', '—'};
	
	public static String[] morseConsoleUnder = 
			new String[morseConsole.length];
	
	public static void main (String[] args) {

		while(true) {
			print("\nWitaj w programie kodującym Tekst<=>Morse. Wybierz opcję:"+
		"\n1.Koduj na alfabet Morse'a\n2.Zamień Morse'a na tekst\n3.Automatyznie rozpoznanie kodowania\n4.Zakończenie programu"+
					"\nTwój wybór: ");
			String w = readInput();
			if (isNumber(w)) {
				int pos = Integer.valueOf(w);
				if (pos<1 || pos>5) {
					print("Dkokonałeś złego wyboru. Spróbuj ponownie!");
					continue;
				}
				switch(pos) {
					case 1:toMorse();
					break;
					case 2:fromMorse();
					break;
					case 3:
					break;
					case 4:
						return;
					default:
						return;
				}
			}
		}
	}
	
	public static void toMorse() {
		print("Podaj ciąg znakowy do przetworzenia na Morse'a: ");
		String m = readInput();
		print("\n\nZakodowany ciąg znakowy: ");
		boolean printed=false;
		for (int i=0;i<m.length();i++) {
			if (((int)m.charAt(i)>=(int)'a' && (int)m.charAt(i)<=(int)'z')) {
				//PRZYKŁAD:			//b -> 98       97
				print(morse[(int)m.charAt(i)-(int)'a'] + "  ");	
				printed=true;
			}
			else if (((int)m.charAt(i)>=(int)'A' && (int)m.charAt(i)<=(int)'Z'))	{	
				print(morse[(int)m.charAt(i)-(int)'A'] + "  ");
				printed=true;
			}
			if (!printed)
				for (int j=0;j<morseStatic.length;j++) {
					if (morseStatic[j][2] == String.valueOf(m.charAt(i))) {
						print(morseStatic[j][0] + "  ");
						printed=true;
						break;
					}
				}
			if (!printed && ((int)m.charAt(i)>=(int)'0' && (int)m.charAt(i)<='9')) {
				String[] numbers = generateMorseNumbers();
				print(numbers[(int)m.charAt(i)-(int)'0'] + " ");
			}
				
			
				
		}
	}
	
	public static String[] generateMorseNumbers() {
		String[] numbers = new String[10];
		for(int j=0;j<6;j++) {
			numbers[j]="";
			int k = 5-j;
			while(k-->0)
				numbers[j]= morseSign[1] + numbers[j];
			while (k++<j-1)
				numbers[j]= morseSign[0] + numbers[j];
		}
		for (int j=1;j<5;j++) {
			numbers[j+5]="";
			int k = 5-j;
			while(k-->0)
				numbers[j+5]= morseSign[0] + numbers[j+5];
			while (k++<j-1)
				numbers[j+5]= morseSign[1] + numbers[j+5];
		}
		return numbers;
	}
	
	public static void fromMorse() {
		for (int i=0;i<morseConsole.length;i++) {
			morseConsoleUnder[i] = morseConsole[i].replace('-', '_');
		}
		print("Podaj kod Morse'a do odkodowania (znaki oddzielaj spacjami): ");
		String m = readInput();
		String[] mtab = m.split(" ");
		print("\n\nOdkodowany ciąg to: ");
		for (String tmp : mtab) {
			for(int i=0;i<morseConsole.length;i++) {
				if (morseConsole[i].compareTo(tmp)==0 ||
						morseConsoleUnder[i].compareTo(tmp)==0) {
					print(String.valueOf((char)(i+(int)'a')));
					break;
				}
				
			}
		}
	}
	
	public static void print(String s) {
		System.out.print(s);
	}
	
	//to jest odpowiednik funkcji nextLine() ze Scanner
	//wczytuje ona znaki z wejscia do chwili pojawienia się znaku
	//nowej linii (ENTER/RETURN); przekazuje przechwycone
	//znaki do zmiennej out i zwraca tą zmienną
	public static String readInput() {
		//zmienna pomocnicza, do ktorej zostaną przekazane wszystkie wczytane
		//znaki
		String out ="";
		//zmienna pomocnicza, do ktrórej wczytywany jest każdy
		//przechwycony znak czy symbol; domyślna wartość -1 mówi
		//że nie ma więcej niczego w buforze
		int c = -1;
		//wczytywanie System.in.read() ZAWSZE musi być w 
		//klauzuli try{} catch{}
		try {
			//pętla ma za zadanie wczytać jażdy przechwycony implus z 
			//z wejścia (pojedynczy bajt) i dopisać go, już jako char,
			//do zmiennej wyjściowej (out); jeżeli wczytany zostanie
			//bajt z wartością 13 (znak nowej linii) pętla przestaje działać
			
			while((c = System.in.read()) != 13) {
				out+=(char)c;
			}
			//obsługa błędu, w tej wersji nieimplementowana 
			//(posiada jedynie domyślną wymaganą obsługę)
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		//zwrócenie wartości
		return out;
	}

	public static boolean isNumber(String s) {
		try {
			
			int test = Integer.valueOf(s);
			if (test < 1 || test > 255)
				return false;
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
}